﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using FluentAssertions;
using InteractiveBooks4Visio.Confluence;

namespace GetAllAttachments.Tests
{
    [TestClass()]
    public class AttachmentHandlerTests
    {
        [TestMethod()]
        public void extractAttachmentInfoTest()
        {
            string json = File.ReadAllText("test.json");
            AttachmentHandler.extractAttachments(json).Should().Contain(
                    "BP Test Dec 5_4522461.vsdx",
                    "/download/attachments/3014732/BP%20Test%20Dec%205_4522461.vsdx?version=1&modificationDate=1512469311007&api=v2")
                .And.HaveCount(43);
        }
    }
}