﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;

namespace InteractiveBooks4Visio.Confluence
{
    public static class AttachmentHandler
    {
        public class confProps
        {
            public string baseUrl;
            public string userName;
            public string password;
            public string auth => Convert.ToBase64String(Encoding.UTF8.GetBytes(userName + ":" + password));
        }

        public static readonly Func<string, string>
        allAttachmentUrl =
           pageId =>
                "/rest/api/content/" + pageId + "/child/attachment?expand=history";

        public static readonly Func<JToken,(string, string, DateTime)>
        extractAttachmentInfo =
            jObject =>
                 ((string) jObject["title"], (string) jObject["_links"]["download"], (DateTime) jObject["history"]["createdDate"]);

        public static readonly Func<JToken, string>
        getNextBatch =
            jObject =>
                (string) jObject["_links"]["next"];


        public static IEnumerable<(String,String,DateTime)> getAttachmentList (confProps props, String restPath, IEnumerable<(String,String,DateTime)> att) 
        {
            using (var client = new WebClient())
            {
                client.Headers.Add("Authorization","Basic "+props.auth);
                var resp = client.DownloadString(props.baseUrl+restPath);

                var attachments = att.Concat(extractAttachments(resp));
                var next = getNextBatch(JObject.Parse(resp));
                if (next is null) return removeDups(attachments);
                else return getAttachmentList(props, next, attachments);
            }
        }
                
        public static Func<IEnumerable<(String,String,DateTime)>,IEnumerable<(String,String,DateTime)>> removeDups =
        list =>
            list
                .GroupBy(d => d.Item1, d => (d.Item1, d.Item2, d.Item3))
                .Select(g => g.Where(r => r.Item3 == g.Max(gr => gr.Item3)).First());
        
        public static readonly Func<string, List<(string, string,DateTime)>>
        extractAttachments =
            json =>
                ((JArray)JObject.Parse(json)["results"]).ToList().Select(
                    j => (extractAttachmentInfo(j).Item1,extractAttachmentInfo(j).Item2,extractAttachmentInfo(j).Item3)).ToList();


        /*public static Dictionary<string, string> 
        getAttachmentList(confProps props, string pageId)
        {
            using (var client = new WebClient())
            {
                client.Headers.Add("Authorization","Basic "+props.auth);
                var resp = client.DownloadString(allAttachmentUrl(props)(pageId));
                var attachments = extractAttachments(resp); 
                return attachments;
            }
        }*/

        public static string downloadAttachment(confProps props, string dirPath, string attachmentUrl)
        {
            var path = Path.GetFullPath(dirPath);
            var fileName = attachmentUrl.Substring(attachmentUrl.LastIndexOf('/') + 1);
            using (var wclient = new WebClient())
            {
                wclient.Headers.Add("Authorization", "Basic " + props.auth);
                Console.WriteLine(fileName,attachmentUrl);
                wclient.DownloadFile(props.baseUrl + attachmentUrl, path + fileName);
            }

            return fileName;
        }

        public static List<string> 
        downloadAllAttachments(confProps props, string dirPath, string pageid)
        {
            var urlList = getAttachmentList(props, allAttachmentUrl(pageid) , new List<(string, string, DateTime)>());
                foreach (var attachment in urlList)
                {
                    downloadAttachment(props, dirPath, attachment.Item2);
                }
            return urlList.Select(u => u.Item1).ToList();
        }
    }
}

