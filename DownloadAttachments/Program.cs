﻿using System;
using InteractiveBooks4Visio.Confluence;


namespace DownloadAttachments
{
    class Program
    {
        static void Main(string[] args)
        {
            var props = new AttachmentHandler.confProps
            {
                baseUrl = args[0],
                userName = args[1],
                password = args[2]
            };
            var list = AttachmentHandler.downloadAllAttachments(
                props, args[3], args[4]);
            Console.WriteLine(list);
        }
    }
}
